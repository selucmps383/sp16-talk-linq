﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINQ_Examples
{
    public class Student
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int Age { get; set; }

        public bool IsMathMajor { get; set; }

        public bool IsCompSciMajor { get; set; }
    }




















   
    public class StudentComparer : IEqualityComparer<Student>
    {

        public bool Equals(Student x, Student y)
        {
            //Check whether the objects are the same object. 
            if (Object.ReferenceEquals(x, y)) return true;

            //Check whether the studentss' properties are equal. 
            return x != null && y != null && x.Name.Equals(y.Name) && x.Age.Equals(y.Age);
        }

        public int GetHashCode(Student student)
        {
            //Get hash code for the Name field if it is not null. 
            int hashStudentName = student.Name == null ? 0 : student.Name.GetHashCode();

            //Get hash code for the Age field. 
            int hashStudentAge = student.Age.GetHashCode();

            //Calculate the hash code for the student. 
            return hashStudentName ^ hashStudentAge;
        }
    }
    
}
