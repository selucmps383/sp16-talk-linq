﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINQ_Examples
{
    class Program
    {
        static void Main(string[] args)
        {
            var numbers = Enumerable.Range(1, 100);

            var evenNumbers = numbers.Where(x => x % 2 == 0);

            var bigOddNumbers = numbers.Where(x => x % 2 != 0  && x > 50);

            var modSeven = numbers.Select(x => x % 7);

            Console.WriteLine("EVEN NUMBERS:\n" + String.Join(", ", evenNumbers) + "\n");

            Console.WriteLine("BIG ODD NUMBERS:\n" + String.Join(", ", bigOddNumbers) + "\n");

            Console.WriteLine("MOD SEVEN:\n" + String.Join(", ", modSeven) + "\n");

            Console.WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n");




            var mystery = numbers.Select(x => x * x);

            var mystery2 = numbers.Where(x => x % 3 != 0).Where(x => x % 2 == 0).Select(x => x * x);

            var mystery3 = numbers.Where(x => x > 20 && x < 80).Select(x => x % 2 == 0);

            Console.WriteLine("MYSTERY #1:\n" + String.Join(", ", mystery) + "\n");

            Console.WriteLine("MYSTERY #2:\n" + String.Join(", ", mystery2) + "\n");

            Console.WriteLine("MYSTERY #3:\n" + String.Join(", ", mystery3) + "\n");

            Console.WriteLine("~~~~~~~~~~~~~BACK TO POWERPOINT~~~~~~~~~~~~~~\n\n");




            IEnumerable<Student> studentList = new List<Student>()
            {
                new Student() {Id = 1, Name = "John", Age = 18, IsCompSciMajor=true, IsMathMajor=false },
                new Student() {Id = 2, Name = "Paul", Age = 22, IsCompSciMajor=true, IsMathMajor=false },
                new Student() {Id = 3, Name = "Amy", Age = 23, IsCompSciMajor=true, IsMathMajor=true },
                new Student() {Id = 4, Name = "Ron", Age = 19, IsCompSciMajor=false, IsMathMajor=true },
                new Student() {Id = 5, Name = "Dana", Age = 20, IsCompSciMajor=false, IsMathMajor=true },
                new Student() {Id = 6, Name = "Ashley", Age = 20, IsCompSciMajor=true, IsMathMajor=false },
                new Student() {Id = 7, Name = "Steven", Age = 18, IsCompSciMajor=true, IsMathMajor=false },
                new Student() {Id = 8, Name = "Kristen", Age = 25, IsCompSciMajor=false, IsMathMajor=true },
                new Student() {Id = 9, Name = "Kimberly", Age = 19, IsCompSciMajor=true, IsMathMajor=false },
                new Student() {Id = 10, Name = "Alicia", Age = 21, IsCompSciMajor=true, IsMathMajor=true }
            };

            var sortedStudentList = studentList.OrderBy(x => x.Age).ThenBy(x => x.Name).Select( x =>"NAME: " + x.Name + " AGE: " + x.Age + "\n");

            Console.WriteLine("STUDENTS:\n\n" + String.Join("\n", sortedStudentList));

            Console.WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n");


            var mathMajors = studentList.Where(x => x.IsMathMajor == true);
            var compSciMajors = studentList.Where(x => x.IsCompSciMajor == true);

            var derivedStudentList = mathMajors.Union(compSciMajors).OrderBy(x => x.Name).Select(x => "NAME: " + x.Name + " AGE: " + x.Age + "\n");

            Console.WriteLine(String.Join("\n", mathMajors.Select(x=>x.Name)));
            Console.WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n");
            Console.WriteLine(String.Join("\n", compSciMajors.Select(x => x.Name)));
            Console.WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n");

            Console.WriteLine("ALPHEBETICAL, NO DUPLICATE STUDENT LIST:\n\n" + String.Join("\n", derivedStudentList));

            Console.WriteLine("That's All Folks!\n~\n~~\n~~~\n~~\n~\n");






















            IEnumerable<Student> studentList2 = new List<Student>()
            {
                new Student() {Id = 1, Name = "John", Age = 18, IsCompSciMajor=true, IsMathMajor=false },
                new Student() {Id = 2, Name = "Paul", Age = 22, IsCompSciMajor=true, IsMathMajor=false },
                new Student() {Id = 3, Name = "Amy", Age = 23, IsCompSciMajor=true, IsMathMajor=true },
                new Student() {Id = 4, Name = "Ron", Age = 19, IsCompSciMajor=false, IsMathMajor=true },
                new Student() {Id = 5, Name = "Dana", Age = 20, IsCompSciMajor=false, IsMathMajor=true },
                new Student() {Id = 6, Name = "Ashley", Age = 20, IsCompSciMajor=true, IsMathMajor=false },
                new Student() {Id = 7, Name = "Steven", Age = 18, IsCompSciMajor=true, IsMathMajor=false },
                new Student() {Id = 8, Name = "Kristen", Age = 25, IsCompSciMajor=false, IsMathMajor=true },
                new Student() {Id = 9, Name = "Kimberly", Age = 19, IsCompSciMajor=true, IsMathMajor=false },
                new Student() {Id = 10, Name = "Alicia", Age = 21, IsCompSciMajor=true, IsMathMajor=true }
            };

            IEnumerable<Student> studentList3 = new List<Student>()
            {
                new Student() {Id = 1, Name = "John", Age = 18, IsCompSciMajor=true, IsMathMajor=false },
                new Student() {Id = 2, Name = "Paul", Age = 22, IsCompSciMajor=true, IsMathMajor=false },
                new Student() {Id = 3, Name = "Amy", Age = 23, IsCompSciMajor=true, IsMathMajor=true },
                new Student() {Id = 4, Name = "Ron", Age = 19, IsCompSciMajor=false, IsMathMajor=true },
                new Student() {Id = 5, Name = "Dana", Age = 20, IsCompSciMajor=false, IsMathMajor=true },
                new Student() {Id = 6, Name = "Ashley", Age = 20, IsCompSciMajor=true, IsMathMajor=false },
                new Student() {Id = 7, Name = "Steven", Age = 18, IsCompSciMajor=true, IsMathMajor=false },
                new Student() {Id = 8, Name = "Kristen", Age = 25, IsCompSciMajor=false, IsMathMajor=true },
                new Student() {Id = 9, Name = "Kimberly", Age = 19, IsCompSciMajor=true, IsMathMajor=false },
                new Student() {Id = 10, Name = "Alicia", Age = 21, IsCompSciMajor=true, IsMathMajor=true }
            };

            var mathMajors2 = studentList2.Where(x => x.IsMathMajor == true);
            var compSciMajors2 = studentList3.Where(x => x.IsCompSciMajor == true);

            Console.WriteLine(String.Join("\n", mathMajors2.Select(x => x.Name)));
            Console.WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n");
            Console.WriteLine(String.Join("\n", compSciMajors2.Select(x => x.Name)));
            Console.WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n");

            var derivedStudentList2 = mathMajors2.Union(compSciMajors2, new StudentComparer()).OrderBy(x => x.Name).Select(x => "NAME: " + x.Name + " AGE: " + x.Age + "\n");

            Console.WriteLine("ALPHEBETICAL, NO DUPLICATE STUDENT LIST:\n\n" + String.Join("\n", derivedStudentList2));

            Console.WriteLine("That's All Folks!");


        }
    }
}
