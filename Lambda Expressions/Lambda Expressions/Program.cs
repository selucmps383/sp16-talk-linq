﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lambda_Expressions
{
    class Program
    {
        static void Main(string[] args)
        {
            Func<int, int> func1 = x => x + 1;

            Func<int, int> func2 = x => x * x;

            Func<int, string> func3 = x => "I am " + x + 5;

            Func<int, int, int> func4 = (x, y) => x * y;

            Func<string, string> func5 = x => x.Remove(5);

            Func<int, bool> func6 = x => x % 2 != 0;

            Func<int, int, int> func7 = 
               (x, y) => { func2(x);
              Console.WriteLine("x=" + x);
              return x + y; };


            Console.WriteLine(func1(2));
            Console.WriteLine(func2(2));
            Console.WriteLine(func3(2));
            Console.WriteLine(func4(2, 3));
            Console.WriteLine(func5("Hello World!!"));
            Console.WriteLine(func6(2));
            Console.WriteLine(func7(2, 3));

            Console.Write("That's it!");
        }
    }
}
